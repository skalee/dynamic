speed = 2

window.iterate = ->
  message = {}

  if keyboard.pressed("left")
    board.player.walk LEFT, speed
  else if keyboard.pressed("right")
    board.player.walk RIGHT, speed
  else if keyboard.pressed("up")
    board.player.walk UP, speed
  else if keyboard.pressed("down")
    board.player.walk DOWN, speed

  if keyboard.pressed("space")
    message.bomb = true

  message.x = board.player.x
  message.y = board.player.y

  client.send message
