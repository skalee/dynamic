Class Interactive is a subclass for all interactive objects on board, that is
everything which can be moved, destroyed etc.

It should be considered and used as an abstract class.

TODO: It is not yet decided whether powerups will inherit from this class.

    class window.Interactive

      constructor: (@x, @y) ->
        @createMesh()
        @

Does some mesh initialization, but must be overriden in subclass to create
actual `@mesh`.  The overriding method should call `super`.

TODO: Actually it should be able to create mesh basing on some sort of
defaults provided by descendant class.

      createMesh: ->
        throw "Override `createMesh` and create actual mesh there." unless @mesh?
        @move @x, @y #TODO
        theatre.scene.add @mesh

Moves mesh to new position in relation to its current position.  Utility
method.

      moveBy: (x, y) ->
        @x = (@mesh.position.x += x)
        @y = (@mesh.position.y += y)

Moves mesh to new position.  Utility method.

      move: (x, y) ->
        @x = @mesh.position.x = x
        @y = @mesh.position.y = y
