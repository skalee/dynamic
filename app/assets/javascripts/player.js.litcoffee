Player class represents player who travel through board.  It aggregates their
meshes, position in theatre, offers means to manipulate them.

    class window.Player extends Interactive

      createMesh: ->
        @mesh = new THREE.Mesh GEOMETRIES.PLAYER, MATERIALS.PLAYER
        @mesh.rotation.x = Math.PI/2
        super()

Calculates movement and moves meshes to new position.
TODO in case of hitting an obstacle, do recursive call from new position.

      walk: (direction, speed) ->

Firstly, move towards nearest X or Y axis (whichever is relevant to
`direction`) if not on it already. If the distance towards the axis is smaller
than value of `speed`, then move in desired direction too.  All that
in recursive calls followed with function's exit points.

        switch direction
          when LEFT, RIGHT
            unless (ytx = @yTowardsX()) is 0
              new_direction = if ytx > 0 then UP else DOWN
              yspeed = Math.abs ytx
              if yspeed > speed
                @walk(new_direction, speed)
              else
                @walk(new_direction, yspeed)
                @walk(direction, speed - yspeed)
              return
          when UP, DOWN
            unless (xty = @xTowardsY()) is 0
              new_direction = if xty > 0 then RIGHT else LEFT
              xspeed = Math.abs xty
              if xspeed > speed
                @walk(new_direction, speed)
              else
                @walk(new_direction, xspeed)
                @walk(direction, speed - xspeed)
              return

If already on proper axis, move in desired direction.

        switch direction
          when LEFT
            @moveBy -speed, 0
          when RIGHT
            @moveBy +speed, 0
          when UP
            @moveBy 0, +speed
          when DOWN
            @moveBy 0, -speed
        @

      yTowardsX: ->
        d = Board.fy * 2
        (0 - @mesh.position.y) % d + d/2

      xTowardsY: ->
        d = Board.fx * 2
        (0 - @mesh.position.x) % d + d/2
