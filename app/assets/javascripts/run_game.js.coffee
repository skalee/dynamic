$ ->
  window.theatre = new Theatre $('#game')
  window.board = new Board
  window.keyboard = new THREEx.KeyboardState
  window.client = new Client received: board.update, connected: everythingReady

everythingReady = ->
  window.theatre.loop()
  window.fpsCounter.displayPeriodicallyOn $('#fps')
