Class `Bomb` represents bomb placed somewhere on board.  There should be
at most one bomb on board's field.  Bomb is intended to explode after given
period of time which will result in destroying boxes and killing players within
range.

    class window.Bomb extends Interactive

      createMesh: ->
        @mesh = new THREE.Mesh GEOMETRIES.BOMB, MATERIALS.BOMB
        super()
