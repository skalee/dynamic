class window.Theatre

  width: 800
  height: 600

  constructor: (@container) ->
    @scene = new THREE.Scene
    @camera = new THREE.OrthographicCamera @width / - 2, @width / 2, @height / 2, @height / - 2, 1, 1000
    @camera.position.z = 80
    @camera.position.x = @width/2 - (@width - 15 * Board.fx)/2 - Board.fx/2
    @camera.position.y = @height/2 - (@height - 13 * Board.fy)/2 - Board.fy/2
    @camera.rotation.x = 0.15
    @scene.add @camera

    @light = new THREE.PointLight 0xFFFFFF
    @light.position.z = 100
    @scene.add @light

    @renderer = new THREE.WebGLRenderer
    @renderer.setSize @width, @height
    @container.append @renderer.domElement

  loop: =>
    window.requestAnimationFrame @loop
    iterate()
    fpsCounter.update()
    @renderer.render @scene, @camera
