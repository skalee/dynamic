# by Phrogz
# http://stackoverflow.com/a/5111475/304175

window.fpsCounter =
  lastFrameAt: new Date()
  averageFrameTime: 0
  fpsCompensation: 50

  update: ->
    thisFrameAt = new Date()
    frameTime = thisFrameAt - @lastFrameAt
    @averageFrameTime += (frameTime - @averageFrameTime) / @fpsCompensation
    @lastFrameAt = thisFrameAt

  get: ->
    if @averageFrameTime is 0
      0
    else
      (1000 / @averageFrameTime).toFixed(1)

  displayOn: (target) ->
    target.html @get() + ' fps'

  displayPeriodicallyOn: (target, interval = 1000) ->
    handler = => @displayOn target
    setInterval handler, interval
