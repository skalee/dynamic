Client is responsible for whole communication with game server.

    class window.Client

      WS_HOST: 'ws://localhost:8080'

      constructor: (options = {}) ->
        @ws = new WebSocket @WS_HOST
        @ws.onopen = options.connected
        @ws.onmessage = (message) ->
          decoded = JSON.parse message.data
          options.received decoded

      send: (message) ->
        encoded = JSON.stringify message
        @ws.send encoded
