class window.Field
  constructor: (@x, @y, @type = EMPTY) ->
    @createMesh()
    @

  createMesh: ->
    switch @type
      when SOLID
        geometry = new THREE.CubeGeometry Board.fx, Board.fy, 40
      else
        null

    if geometry?
      @mesh = new THREE.Mesh geometry, MATERIALS[@type]
      @mesh.position.x = @x * Board.fx
      @mesh.position.y = @y * Board.fy
      theatre.scene.add @mesh
    else
      @mesh = null
