class window.Board
  height: 13
  width: 15

  @fx: 40
  @fy: 40

  constructor: ->
    @my = @height - 1
    @mx = @width - 1

    @fields = new Array @height
    (@fields[y] = new Array @width) for y in [0..@my]

    @setUp()
    @createFloor()
    @player = new Player 40, 120
    @rivals = []

  eachField: (fun) ->
    for row, y in @fields
      for field, x in row
        fun field, x, y

  setUp: ->
    @eachField (_, x, y) =>
      if y is 0 or y is @my or x is 0 or x is @mx # border
        type = SOLID
      else if y % 2 is 0 and x % 2 is 0 # inner solid
        type = SOLID
      else
        type = EMPTY
      @fields[y][x] = new Field(x, y, type)

  createFloor: ->
    geometry = new THREE.PlaneGeometry 2000, 2000
    material = new THREE.MeshBasicMaterial color: 0xCCCC00
    mesh = new THREE.Mesh geometry, material
    mesh.position.z = -20
    theatre.scene.add mesh

  update: (msg) =>
    while msg.rivals.length > @rivals.length
      rival = new Player 0, 0
      @rivals.push rival

    for update, i in msg.rivals
      @rivals[i].move update.x, update.y

    for coords in msg.bombs
      if @fields[coords.y][coords.x].type != BOMB
        @fields[coords.y][coords.x].type = BOMB
        new Bomb coords.x * Board.fx, coords.y * Board.fy
