Introduce temporary object whose properties will be eventually merged into
`this` (`window`).

    constants =

For performance reasons we use constants instead of strings.  (Decision was
made in very early stage; it's possible that it's not necessary at all).
Following names refer to types of board fields.

TODO: The idea of field types is _very_ disputable.  It does not refer to
`Interactive` which may present there at all.  It's just a pure, meaningless
value.  May be drastically changed in future, probably removed.

      EMPTY: 0
      SOLID: 1
      BOMB:  2

Following refer to directions on board.

      LEFT: 100
      RIGHT: 101
      UP: 102
      DOWN: 103

And materials used on board.  Numbers refer to above field types (TODO rewrite
in nicer fashion).

      MATERIALS:
        1: (new THREE.MeshLambertMaterial vertexColors: THREE.FaceColors)
        PLAYER: (new THREE.MeshLambertMaterial color: 0x0000CC)
        BOMB: (new THREE.MeshLambertMaterial color: 0xCCCCCC)

And geometries:

      GEOMETRIES:
        PLAYER: (new THREE.CylinderGeometry 18, 18, 60, 70)
        BOMB: (new THREE.SphereGeometry 20, 90)

Finally, merge into `this` as said earlier.

    for own name, value of constants
      this[name] = value
