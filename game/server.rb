require 'rubygems'
require 'bundler/setup'
require 'reel'
require 'pry'
require 'json'

FX, FY = 40, 40

class Board
  include Celluloid
  include Celluloid::Logger

  Player = Struct.new(:x, :y)

  def initialize
    info "New board created"
    @players = Hash.new
    @board = Hash.new
  end

  # client_id identifies Connection actor and, consequently, identifies player.
  def update client_id, msg
    player = (@players[client_id] ||= Player.new)
    response = Hash.new
    move player, msg
    try_planting_bomb player, msg
    response[:player] = obtain_only player
    response[:rivals] = obtain_except player
    response[:bombs] = @board.each_pair.inject([]) do |acc, coords_and_state|
      coords, state = coords_and_state
      acc.push(x: coords[0], y: coords[1]) if state == :bomb
    end
    response
  end

  private

  def position_to_board_index msg
    [(msg['x'] + FX/2) / FX, (msg['y'] + FY/2) / FY].map(&:to_i)
  end

  def move player, msg
    player.x = msg['x']
    player.y = msg['y']
  end

  def try_planting_bomb player, msg
    return unless msg['bomb']
    @board[position_to_board_index msg] ||= :bomb
  end

  def obtain_only player
    {x: player.x, y: player.y}
  end

  def obtain_except player
    @players.values.reject(&player.method(:equal?)).map(&method(:obtain_only))
  end
end

class Connection
  include Celluloid
  include Celluloid::Logger

  def initialize ws
    info "Received a WebSocket connection"
    @ws = ws
  end

  def listen
    loop do
      process_message @ws.read #TODO not sure if #read is blocking; possibly #read_every would be better
    end
  rescue IOError
    info "Client has disconnected"
    terminate
  ensure
    @ws.close
  end

  def process_message msg
    parsed_msg = JSON.parse msg
    new_state = board.update self.object_id, parsed_msg
    @ws << new_state.to_json
  end

  def board
    Actor[:board]
  end
end

class GameServer < Reel::Server
  include Celluloid::Logger

  @clients = []

  def initialize(host = "127.0.0.1", port = 8080)
    info "Game server starting on #{host}:#{port}"
    super(host, port, &method(:on_connection))
  end

  def on_connection(connection)
    while request = connection.request
      case request
      when Reel::WebSocket
        Connection.new(request).listen
      end
    end
  end
end

Board.supervise_as :board
GameServer.supervise_as :reel

sleep
